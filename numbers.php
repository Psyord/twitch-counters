<?php
session_start();

if (file_exists('token.php')) {
    require 'token.php';
}
else {
    print 'You need to access secret stuff first ;)';
    exit;
}

require 'Counter.php';
require 'config.php';

$c = new Counters($clientId, $clientSecret);
$c->setAccessToken($accessToken);

// ------------------------------------

$channel = 'protech';

try {
    if (empty($_SESSION['streamInfo'])) {
        $_SESSION['streamInfo'] = $c->getStreamInfo($channel);
    }
    elseif ($_SESSION['streamInfo']['lastUpdate']+600 < time()) {
        $_SESSION['streamInfo'] = $c->getStreamInfo($channel);
    }
    $stream = (object)$_SESSION['streamInfo'];
} catch (Exception $e) {
    printf('<p>Error: %s</p>', $e->getMessage());
    unset($_SESSION['streamInfo']);
    exit; // Do not print anything if error'd
}
