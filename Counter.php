<?php
class Counters {

    protected $accessToken;
    protected $tokenCode;
    
    protected $clientId;
    protected $clientSecret;
    
    public function __construct($clientId, $clientSecret) {
    
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
   
    }
    
    protected function generateServerUrl() {
        
        return sprintf('http://%s%s', $_SERVER['HTTP_HOST'], $_SERVER['SCRIPT_NAME']);
    
    }
    
      
    public function getAuthorizeCode(array $scope = []) {
             
        $params = http_build_query([
        
            'response_type' => 'code',
            'client_id'     => $this->clientId,
            'scope'         => implode(' ', $scope),
            'redirect_uri'  => $this->generateServerUrl(),
            //'state'         => 'trololo'
        ]);

        $options = [
            'http' => [
                'method' => 'GET',
                'ignore_errors' => true
            ]
        ];
               
        $url = 'https://api.twitch.tv/kraken/oauth2/authorize?' . $params;
               
        header('Location: '. $url);
    
    }
    
    public function setTokenCode($code) {
        
        $this->tokenCode = $code;
        
    }
    
    public function setAccessToken($token) {
    
        $this->accessToken = $token;
        
    }
    
    public function getToken() {
      
        $tokenUrl = 'https://api.twitch.tv/kraken/oauth2/token';
        
        $params = http_build_query([
        
            'grant_type'    => 'authorization_code',
            'client_id'     => $this->clientId,
            'client_secret' => $this->clientSecret,
            'redirect_uri'  => $this->generateServerUrl(),
            'code'          => $this->tokenCode
        ]);
        
        $options = [
            'http' => [
                'method' => 'POST',
                'header'=> "Content-type: application/x-www-form-urlencoded\r\nAccept: application/vnd.twitchtv.v2+json",
                'content' => $params,
                'ignore_errors' => true
            ]
        ];
        
      
        $response = file_get_contents($tokenUrl, false, stream_context_create($options));
        
        $data = json_decode($response);
        
        if (isset($data->access_token)) {       
            return $this->accessToken = json_decode($response)->access_token;
        }
        else {
            throw new Exception($data->message);
        }
    
    }

    public function makeRequest($url, $method = 'GET', array $parameters = [], $requiresToken = false) {
    
        $options = [
            'http' => [
                'method' => 'GET',
                'header'=> "Accept: application/vnd.twitchtv.v2+json\r\nClient-ID: {$this->clientId}\r\n",
                'ignore_errors' => true
            ]
        ];
    
        switch ($method) { // TODO: PUT is not supported... yet
            
            case 'POST':
                $options['http']['method'] = 'POST';
                $options['http']['header'] .= "Content-type: application/x-www-form-urlencoded\r\n";
                $options['http']['content'] = http_build_query($parameters);
                break;
                
            case 'GET':
                $url .= '?' . http_build_query($parameters);
                break;
        
        }
        
        if ($requiresToken) {
            $options['http']['header'] .= sprintf("Authorization: OAuth %s\r\n", $this->accessToken);
        }
    

        $response = file_get_contents($url, false, stream_context_create($options));
        return json_decode($response);
        
    }

    public function getFollowers($channel) {
        $data = $this->makeRequest(sprintf('https://api.twitch.tv/kraken/channels/%s/follows', $channel));
        
        return $data->_total;
    }
    
    protected function getTotalSubscribers($data) {
        
        return $data->_total;

    }

    protected function getRecentSubscribers($data, $limit = 2) {
    
            $list = [];
            foreach ($data->subscriptions as $i => $row) {
                $list[] = $row->user->name;
                if ($i+1 >= $limit) break;
            }
        
            return $list;
            
    }
    
    protected function getWeeklySubscribers($data) {
  
        $start = new DateTime('now', new DateTimeZone('America/New_York'));
        $start->modify('00:00:00 this week -1 day'); // Week starts from sunday in this case
        $end = clone $start;
        $end->modify('+6 days 23:59:59');
        
        $subCount = 0;
        foreach ($data->subscriptions as $row) {
            $subTime = new DateTime($row->created_at);
            if ($subTime > $start && $subTime < $end) {
                $subCount++;
            }
        }
        return $subCount;
        
    }
    
    public function getSubscriptionData($channel) {
        $url = sprintf('https://api.twitch.tv/kraken/channels/%s/subscriptions', $channel);
        $data = $this->makeRequest($url, 'GET', ['limit' => 50, 'direction' => 'desc'], true);
        
        if (isset($data->error)) {
            throw new Exception($data->message);
        }
        else {
            
            $list = [
                'totalSubs'      => $this->getTotalSubscribers($data),
                'recentSubs'     => $this->getRecentSubscribers($data, 2),
                'weeklySubCount' => $this->getWeeklySubscribers($data),
            ];
        
            return $list;
        }
    }
    
    public function getStreamInfo($channel) {
        
        $info = $this->getSubscriptionData($channel);
        $info['followersCount'] = $this->getFollowers($channel);
        $info['lastUpdate'] = time();
        
        return $info;
        
    }
    
}

function dealWithTokens(Counters $c) {

    /*
        Flow:
        1) redirect user to twitch login
        2) twitch will redirect user back with code
        3) Make request to twitch with code and get access code
    */

    if (!empty($_SESSION['accessToken'])) { // We have cached token, nothing needs to be done
        $c->setAccessToken($_SESSION['accessToken']);
    }

    elseif (isset($_GET['code'])) { // User is redirected from twitch at this point
    
        $_SESSION['tokenCode'] = $_GET['code']; // Set to get access token
        $c->setTokenCOde($_GET['code']);
    
        $_SESSION['accessToken'] = $c->getToken(); // Used to call api
        
        header('Location: index.php'); // Totally useless, but why not
    
    }

    if (!empty($_SESSION['tokenCode'])) { // We have code to get access token
        $c->setTokenCode($_SESSION['tokenCode']);
    }
    
    else { // We have nothing, get the code
        $c->getAuthorizeCode(['channel_subscriptions']);
    }
}