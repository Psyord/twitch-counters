<?php
// No need to touch these
$clientId = '';
$clientSecret = '';
$sitePassword = sha1(uniqid(serialize($_SERVER), true));

session_start();

if (isset($_GET['reset'])) {
    session_destroy();
    header('Location: auth.php');
    exit;
}

require 'Counter.php';
require 'config.php';

$c = new Counters($clientId, $clientSecret);

if (empty($_POST['password'])) {
    
    print '<html><head><title>Token fetcher</title></head><body><form id="login" method="POST" action=""><input type="password" name="password"><input type="submit"></form></body></html>';

}
elseif ($_POST['password'] == $sitePassword) {
    dealWithTokens($c);
    file_put_contents('token.php', sprintf('<?php $accessToken = %s;', var_export($_SESSION['accessToken'], true)));
    print 'Secret stuff is now <strong>SET</strong>. <a href="index.php">Back to numbers.</a>';
}
else {
    print 'Password is required...';
}