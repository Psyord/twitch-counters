<?php require 'numbers.php'; ?><!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Numbers</title>
    <style>
    
    p {
        font-family: serif;
    }
    .names {
        word-spacing: 5px;
    }
    </style>
</head>

<body>

  <p>Followers: <?php print $stream->followersCount; ?> / 200000</p>
  <p>Subscriptions: <?php print $stream->totalSubs; ?> / 200</p>
  <p>Recent Subscribers: <span class="names"><?php print implode(', ', $stream->recentSubs); ?></span></p>
  <p>This week Subscribers: <?php print $stream->weeklySubCount; ?></p>
  
</body>
</html>